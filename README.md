#### QTILE [XORG / WAYLAND]

> terminal		: **[alacritty](https://github.com/eendroroy/alacritty-theme)**
 
> shell			: **[zsh](https://github.com/dikiaap/dotfiles/blob/master/.oh-my-zsh/themes/oxide.zsh-theme)**
   
![img](assets/terminal.png)

#### BSPWM [XORG]

> start X server	: **sx**

> system monitor	: **conky**
                    
![img](assets/wall1.png)

![img](assets/wall2.png)

> text editor		: **[nvim](https://github.com/yutyo/vimrc) without plugin**

![img](assets/nvim.png)

> browser		: **[qutebrowser](https://github.com/theova/base16-qutebrowser)**

![img](assets/qutebrowser.png)

> Multimedia		: **ffmpeg**

> Wayland Screencap	: **wf-recorder, grim, slurp**

> Xorg Screencap	: **ffmpeg, slop**

> Font			: **[hurmit nerd](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hermit)**

***highlighted in BLUE are link to original work***
